﻿using FunWithTaxis.Api.Validation;
using FunWithTaxis.Entities;
using FunWithTaxis.ObjectMapper;
using FunWithTaxis.Repository;
using FunWithTaxis.Repository.Exceptions;
using NLog;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FunWithTaxis.Api.Controllers
{
    public class DriversController : ApiController
    {
        private DriverService _driverService;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public DriversController()
        {
            var repository = new TaxisRepository(new TaxisContext());
            repository.SeedWithSampleData();
            _driverService = new DriverService(repository, new DtoMapper());
        }

        [Route("api/drivers/inArea")]
        public HttpResponseMessage Get(string ne, string sw)
        {
            return ProcessRequest(() =>
            {
                return _driverService.ListAvalilableDrivers(new BoundingBoxDTO(new Coordinate(ne), new Coordinate(sw)));
            });
        }

        [Route("api/drivers/{id}/status")]
        [Validate]
        public HttpResponseMessage Put(int id, [FromBody]DriverStatusDTO status)
        {
            return ProcessRequest(() =>
            {
                status.DriverId = id;
                return _driverService.UpdateDriverStatus(status);
            });
        }

        private HttpResponseMessage ProcessRequest( Func<object> request )
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, request());
            }
            catch (EntityInvalidArgumentException argException)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, argException.Message);
            }
            catch (EntityNotFoundException enfEx)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, enfEx.Message);
            }
            catch (Exception e)
            {
                var errorGuid = new { status = "error", guid = Guid.NewGuid().ToString() };
                var errorObject = new
                {
                    Guid = errorGuid.guid,
                    Request = Request.RequestUri.ToString()
                };
                
                _logger.Error(e, Newtonsoft.Json.JsonConvert.SerializeObject(errorObject));
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, Newtonsoft.Json.JsonConvert.SerializeObject(errorGuid));
            }
        }

    }
}
