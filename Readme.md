# FunWithTaxis
Just some fun with a simple RESTfull Web API to handle taxis location and availability

## Overview

### WebAPI

**Get available drivers in area (bounding box)**
```sh
$ curl "http://localhost:57679/api/drivers/inArea?ne=-20.757752,-42.357777&sw=-25.2114965,-47.647694"
$ [{"latitude":-23.550462867257721,"longitude":-46.638315772578132,"driverId":1},{"latitude":-23.550462867257721,"longitude":-46.638315772578132,"driverId":2},{"latitude":-22.904985200572707,"longitude":-43.216082681249176,"driverId":5}]
```
**Update driver status**
```sh
$ curl -H "Content-Type: application/json" -X PUT -d {"available":false,"latitude":-23.5504628,"longitude":-46.6383157} http://localhost:57679/api/drivers/1/status
$ {"available":false,"latitude":-23.5504628,"longitude":-46.6383157,"driverId":1}
```

#### Expected Status Codes
**200 - OK**

**500 - Internal Server Error**

**400 - Bad Request** >> Common examples: 

* Invalid query string parameters
* Invalid request format (example: lat,lng)

**404 - Not Found** >> Common examples: 

* Resource not found
* Uri not found


## Objectives
This solution is definitely simple, actually I'd like to propose a more complex solution but it'd take the time I currently don't have, and then compromise some basic concepts such as testing.

Then, what is the real objective of the proposed solution:

* Code quality

    * SOLID
    * DRY

* Testing

    * Not exhaustive testing, just basic functional tests in critical resources

* Extensibility

    * E.g: Changing repository to include a memory caching layer

**What isn't the objective**

* High complexity solution
* High performance

## Solution description

Although the projects names are pretty clear, bellow the description of each one.

* FunWithTaxis: Core project, owns the business logic (not so much for this simple solution)
* FunWithTaxis.Repository: Persistence layer
* FunWithTaxis.Api: RESTful Web API which exposes the "core" functionalities via web.
* Tests
    * FunWithTaxis.Repository.Tests: Basic functional tests for the Repository project
    * FunWithTaxis.Tests: Basic functional tests for the "core" project

*Database*: localdb (Lighweight version of SQL Server Express), accessed through EntityFramework

### Dependencies

    AutoMapper - Object mapping
    EntityFramework - Persistence ORM Framework
    NLog - Logging
    Microsoft.AspNet.WebApi - WebAPI framework
    Newtonsoft.Json - Json parsing
    FluentAssertions - Clearer test assertions in "fluent" specifications

## Improvements
### Performance

Introduce high performance persistence layer for recent taxis (last seen in less than XX minutes/hours)

#### **Redis**

My first suggestion for a prototype testing would be Redis, which in the latest version introduced spatial indexes (see http://redis.io/commands/geoadd).

A suggestion for the first approach in the workflow for requesting taxis information would be:

    API > Core > HighPerfPersistence > TraditionalPersistence
Asyncronous services would periodically update "TraditionalPersistence" layer with the "HighPerfPersistence" data

TTL specification in Redis would help to keep the memory consumption more under control (sort of shortcut to last seen constraint), without expending muuch time handling manually the persisted registries.

The persistence layer would deal with the "problem" of querying for the HighPerf database pool or TraditionalPersistence pool when necessary (not found in first option, for example).

### Testing

* Introduce more unit tests
* Introduce acceptance tests (tests performed agains real hosted APIs, with human readable specification using, for example, Specflow **Specification by Example**

### Scala // Node

My first intentions were to develop this solution using Scala (or even Javascript with Node), but again, the free time I've had free the last months to spend in developing this was really short, so I'd probably end up with a low quality solution since it'll be about languages and frameworks which are not my "fluent" ones.

## Running

All dependencies are managed through nuget, so them will be restored automatically at the first build. Just be sure your VisualStudio configurations are set to allow automatic package restore (https://docs.nuget.org/consume/package-restore)

### Tests
Just hit **Tests > Run > All tests** in VisualStudio

Tests were made using MSTest Framework and FluentAssertions assertion library.

### Web API
Just hit **Execute** in VisualStudio

The base URL for the API is: **localhost:57679/api/**