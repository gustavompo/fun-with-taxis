﻿using FluentAssertions;
using FunWithTaxis.Entities;
using FunWithTaxis.ObjectMapper;
using FunWithTaxis.Repository;
using FunWithTaxis.Repository.Seeder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace FunWithTaxis.Tests.Integration
{
    [TestClass]
    public class DriverServiceTest
    {
        private DriverService _service;

        [TestInitialize]
        public void Init()
        {
            var repository = new TaxisRepository(new TaxisContext());
            repository.SeedWithSampleData();
            _service = new DriverService(repository, new DtoMapper());
        }

        [TestCleanup]
        public void ClenUp()
        {
            _service.Repository.Context.Dispose();
        }

        [TestMethod]
        public void DriverServiceUpdateStatus_ValidDriverStatus_UpdatedSuccessfully()
        {
            _service.UpdateDriverStatus(new DriverStatusDTO { Latitude = -11, Longitude = -22, Available = true, DriverId = 1 });
            var updated = _service.Repository.Context.DriversStatuses.FirstOrDefault(e => e.DriverId == 1);
            updated
                .Should()
                .NotBeNull()
                .And
                .Match<Repository.Model.DriverStatus>(e => e.Available && e.Latitude == -11 && e.Longitude == -22);
                
        }

        [TestMethod]
        public void DriverServiceListDrivers_BBoxCoveringSpAndRio_AvailableDriversInSpAndRio()
        {
            var spAndRioBbox = new BoundingBoxDTO(-20.757752, -25.2114965, -42.357777, -47.647694);
            var drivers = _service.ListAvalilableDrivers(spAndRioBbox).ToList();
            drivers
                .Should()
                .NotBeNullOrEmpty()
                .And
                .OnlyContain(e => FunnyTaxisDatabaseSeeder.DriversStatuses.Any(stat => stat.Available && stat.DriverId == e.DriverId));
        }

        [TestMethod]
        public void DriverServiceListDrivers_BBoxCoveringFortaleza_NoDrivers()
        {
            var spAndRioBbox = new BoundingBoxDTO(-3.530262, -4.1338422, -38.197398, -38.858638);
            var drivers = _service.ListAvalilableDrivers(spAndRioBbox);
            drivers
                .Should()
                .BeEmpty();
        }
    }
}
