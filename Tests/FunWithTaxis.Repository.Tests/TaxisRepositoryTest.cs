﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
using FunWithTaxis.Repository.Seeder;
using FluentAssertions;
using FunWithTaxis.Repository.Model;
using System.Linq;

namespace FunWithTaxis.Repository.Tests
{
    [TestClass]
    public class TaxisRepositoryTest
    {
        private TaxisRepository _repository;

        public TaxisRepositoryTest()
        {
            Database.SetInitializer(new FunnyTaxisDatabaseSeeder());
        }

        [TestInitialize]
        public void Init()
        {
            _repository = new TaxisRepository(new TaxisContext());
        }

        [TestCleanup]
        public void CleanUp()
        {
            _repository.Context.Dispose();
        }

        [TestMethod]
        public void TaxisRepositoryInsertion_NewDriver_AddedDriverIdIsGreaterThanZero()
        {
            var addedDriver = _repository.AddDriver(new Model.Driver { CarPlate = "FUN-2485", Name = "Dr1v3r" });
            addedDriver
                    .Should()
                    .NotBeNull()
                    .And
                    .Match<Driver>(e => e.DriverId > 0, "added driver id must be greater than zero");
        }

        [TestMethod]
        public void TaxisRepositoryQuery_ExistentDriverId_ExpectedDriver()
        {
            var driver = _repository.GetDriver(1);
            var expectedCarPlate = FunnyTaxisDatabaseSeeder.DriversToSeed.First(e => e.DriverId == 1).CarPlate;
            driver
                .Should()
                .NotBeNull()
                .And
                .Match<Driver>(e => e.CarPlate == expectedCarPlate);
            
        }

        [TestMethod]
        public void TaxisRepositoryQuery_ExistentDriverId_ExpectedDriverStatus()
        {
            var driverStatus = _repository.GetDriverStatusForDriver(1);
            driverStatus
                .Should()
                .NotBeNull()
                .And
                .Match<DriverStatus>(e => e.DriverId == 1);
        }

        [TestMethod]
        public void TaxisRepositoryQuery_PredicateForAllDrivers_AllDriversInSeeder()
        {
            var allDrivers = _repository.ListDrivers(e => true).ToList();
            allDrivers
                .Should()
                .Match(allAdded => FunnyTaxisDatabaseSeeder.DriversToSeed.All(seed => allAdded.Any(added => added.CarPlate == seed.CarPlate)));

        }

        [TestMethod]
        public void TaxisRepositoryQuery_PredicateForAllDriversStatuses_AllDriversStatusesInSeeder()
        {
            var allDriversStatuses = _repository.ListDriverStatuses(e => true).ToList();
            allDriversStatuses
                .Should()
                .Match(allAdded => FunnyTaxisDatabaseSeeder.DriversToSeed.All(seed => allAdded.Any(added => added.DriverId == seed.DriverId)));
        }

        [TestMethod]
        public void TaxisRepositoryInsertion_NewDriverStatus_DriverStatusLastSeenMatchesTheExpected()
        {
            var now = DateTime.Now;
            var updatedStatus = _repository.AddOrUpdateDriverStatus(new DriverStatus { Available = true, DriverId = 1, Latitude = -26, Longitude = -46, LastSeen = now });
            updatedStatus
                    .Should()
                    .NotBeNull()
                    .And
                    .Match<DriverStatus>(e => e.LastSeen == now);
        }

    }
}
