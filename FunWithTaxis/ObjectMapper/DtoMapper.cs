﻿using AutoMapper;
using FunWithTaxis.Entities;
using FunWithTaxis.Repository.Model;

namespace FunWithTaxis.ObjectMapper
{
    public interface IDtoMapper
    {
        U MapTo<U>(object origin);
    }

    public class DtoMapper : IDtoMapper
    {
        public DtoMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Driver, DriverDTO>();
                cfg.CreateMap<DriverStatus, DriverStatusDTO>();
                cfg.CreateMap<DriverDTO, Driver>();
                cfg.CreateMap<DriverStatusDTO, DriverStatus>();
            });
            
        }

        public U MapTo<U>(object origin)
        {
            return Mapper.Map<U>(origin);
        }
    }
}
