﻿using FunWithTaxis.Entities;
using FunWithTaxis.ObjectMapper;
using FunWithTaxis.Repository;
using FunWithTaxis.Repository.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FunWithTaxis
{
    public interface IDriverService
    {
        DriverStatusDTO UpdateDriverStatus(DriverStatusDTO status);
        IEnumerable<DriverStatusDTO> ListAvalilableDrivers(BoundingBoxDTO bbox);
    }

    public class DriverService : IDriverService
    {
        public IDtoMapper Mapper { get; private set; }
        public ITaxisRepository Repository { get; private set; }

        public DriverService(ITaxisRepository repository, IDtoMapper mapper)
        {
            Repository = repository;
            Mapper = mapper;
        }

        public DriverStatusDTO UpdateDriverStatus(DriverStatusDTO statusDto)
        {
            var status = Mapper.MapTo<DriverStatus>(statusDto);
            status.LastSeen = DateTime.Now;
            var updatedStatus = Repository.AddOrUpdateDriverStatus(status);
            return Mapper.MapTo<DriverStatusDTO>(updatedStatus);
        }

        public IEnumerable<DriverStatusDTO> ListAvalilableDrivers(BoundingBoxDTO bbox)
        {
            var drivers = Repository.ListDriverStatuses(e =>
               e.Available &&
               e.Latitude <= bbox.NorthEast.Latitude && e.Latitude >= bbox.SouthWest.Latitude &&
               e.Longitude <= bbox.NorthEast.Longitude && e.Longitude >= bbox.SouthWest.Longitude);

            return drivers.Select(e =>
            {
                var result = Mapper.MapTo<DriverStatusDTO>(e);
                result.Available = null;
                return result;
            });
        }
    }
}
