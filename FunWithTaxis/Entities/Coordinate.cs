﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace FunWithTaxis.Entities
{
    public class Coordinate
    {
        private IFormatProvider _enUsCulture;
        private static Regex _coordinateRegex = new Regex(@"^(?<lat>(\-?\d+(\.\d+)?)),\s*(?<lng>(\-?\d+(\.\d+)?))$");
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }

        public Coordinate(double latitude, double longitude)
        {
            Init(latitude, longitude);
        }

        public Coordinate(string latlng)
        {
            var match = _coordinateRegex.Match(latlng);
            if (!match.Success)
                throw new EntityInvalidArgumentException(string.Format("invalid latlng format. expected=[-nn.nnn,-nn.nnn], actual=[{0}]", latlng));

            Init(double.Parse(match.Groups["lat"].Value, _enUsCulture), double.Parse(match.Groups["lng"].Value, _enUsCulture));
        }

        private void Init(double latitude, double longitude)
        {
            _enUsCulture = new CultureInfo("en-US");
            if (latitude > 90 || latitude < -90)
                throw new EntityInvalidArgumentException(string.Format("latitude must be greater than -90 and lower than 90 degrees. value [{0}]", latitude));
            if (longitude > 180 || longitude < -180)
                throw new EntityInvalidArgumentException(string.Format("longitude must be greater than -180 and lower than 180 degrees. value [{0}]", longitude));

            Latitude = latitude;
            Longitude = longitude;
        }

    }
}