﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FunWithTaxis.Entities
{
  
    public class DriverStatusDTO : IValidatableObject
    {
        public bool? Available { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int DriverId { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            try
            {
                new Coordinate(Latitude, Longitude);
            }
            catch (EntityInvalidArgumentException argEx)
            {
                return new[] { new ValidationResult(argEx.Message) };
            }
            return new List<ValidationResult>();
        }
    }
}
