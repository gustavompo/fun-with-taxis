﻿using System;
using System.Runtime.Serialization;

namespace FunWithTaxis.Entities
{
    [Serializable]
    public class EntityInvalidArgumentException : Exception
    {

        public EntityInvalidArgumentException(string message) : base(message)
        {
        }

        public EntityInvalidArgumentException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}