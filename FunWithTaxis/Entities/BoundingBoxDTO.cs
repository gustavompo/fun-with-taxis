﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FunWithTaxis.Entities
{
    public class BoundingBoxDTO
    {
        public BoundingBoxDTO(double north, double south, double east, double west)
        {
            NorthEast = new Coordinate(north, east);
            SouthWest = new Coordinate(south, west);
        }

        public BoundingBoxDTO(Coordinate northEast, Coordinate southWest)
        {
            NorthEast = northEast;
            SouthWest = southWest;
        }

        public Coordinate NorthEast { get; private set; }
        public Coordinate SouthWest { get; private set; }
    }
}
