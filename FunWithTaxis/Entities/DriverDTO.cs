﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FunWithTaxis.Entities
{
    public class DriverDTO
    {
        public int DriverId { get; set; }
        public string CarPlate { get; set; }
    }
}
