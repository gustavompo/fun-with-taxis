﻿using FunWithTaxis.Repository.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace FunWithTaxis.Repository
{
    public class TaxisContext : DbContext 
    {
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<DriverStatus> DriversStatuses { get; set; }
    }
}
