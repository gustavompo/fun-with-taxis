﻿using FunWithTaxis.Repository.Exceptions;
using FunWithTaxis.Repository.Model;
using FunWithTaxis.Repository.Seeder;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace FunWithTaxis.Repository
{
    public interface ITaxisRepository
    {
        TaxisContext Context { get; }
        Driver GetDriver(int id);
        DriverStatus GetDriverStatusForDriver(int id);

        IEnumerable<Driver> ListDrivers(Expression<Func<Driver, bool>> predicate);
        IEnumerable<DriverStatus> ListDriverStatuses(Expression<Func<DriverStatus, bool>> predicate);

        Driver AddDriver(Driver driver);
        DriverStatus AddOrUpdateDriverStatus(DriverStatus driverStatus);
        void SeedWithSampleData();
    }

    public class TaxisRepository : ITaxisRepository
    {
        public TaxisContext Context { get; private set; }

        public TaxisRepository(TaxisContext context)
        {
            Context = context;
        }

        public void SeedWithSampleData()
        {
            Database.SetInitializer(new FunnyTaxisDatabaseSeeder());
        }

        public Driver GetDriver(int id)
        {
            return Context.Drivers.FirstOrDefault(e => e.DriverId == id);
        }

        public DriverStatus GetDriverStatusForDriver(int driverIdd)
        {
            return Context.DriversStatuses.FirstOrDefault(e => e.DriverId == driverIdd);
        }

        public IEnumerable<Driver> ListDrivers(Expression<Func<Driver, bool>> predicate)
        {
            return Context.Drivers.Where(predicate);
        }

        public IEnumerable<DriverStatus> ListDriverStatuses(Expression<Func<DriverStatus, bool>> predicate)
        {
            return Context.DriversStatuses.Where(predicate);
        }

        public Driver AddDriver(Driver driver)
        {
            var addedDriver = Context.Drivers.Add(driver);
            Context.SaveChanges();
            return addedDriver;
        }

        public DriverStatus AddOrUpdateDriverStatus(DriverStatus driverStatus)
        {
            var status = Context.DriversStatuses.Find(driverStatus.DriverId);

            if(status == null)
            {
                var driver = Context.Drivers.Find(driverStatus.DriverId);
                if (driver == null)
                    throw new EntityNotFoundException(string.Format("driver with id=[{0}] was not found", driverStatus.DriverId));
                status = Context.DriversStatuses.Add(driverStatus);
            }
            else
                status.Update(driverStatus);

            Context.SaveChanges();
            return status;
        }
    }
}
