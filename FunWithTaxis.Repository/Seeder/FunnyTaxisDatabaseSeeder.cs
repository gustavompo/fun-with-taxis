﻿using FunWithTaxis.Repository.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace FunWithTaxis.Repository.Seeder
{
    public class FunnyTaxisDatabaseSeeder : DropCreateDatabaseAlways<TaxisContext>
    {
        private static Tuple<double,double> _spLatLng = new Tuple<double,double>(-23.55046286725772, -46.63831577257813);
        private static Tuple<double,double> _rjLatLnt = new Tuple<double,double>(-22.904985200572707, -43.216082681249176);
        private static DateTime _tenMinutesAgo = DateTime.Now.Subtract(TimeSpan.FromMinutes(10));
        private static DateTime _threeHoursAgo = DateTime.Now.Subtract(TimeSpan.FromHours(3));


        public static List<Driver> DriversToSeed = new[]
        {
            new Model.Driver { DriverId = 1, CarPlate = "SER-1055", Name = "Serious driver" },
            new Model.Driver { DriverId = 2, CarPlate = "NOT-5321", Name = "Not so serious driver" },
            new Model.Driver { DriverId = 3, CarPlate = "FUN-1337", Name = "Funny driver" },
            new Model.Driver { DriverId = 4, CarPlate = "ANG-2100", Name = "Angry driver" },
            new Model.Driver { DriverId = 5, CarPlate = "CRA-7100", Name = "Crazy driver" }
        }.ToList();

        public static List<DriverStatus> DriversStatuses = new[]
        {
            new Model.DriverStatus { DriverId = 1, Available = true,  LastSeen = _tenMinutesAgo, Latitude =_spLatLng.Item1, Longitude = _spLatLng.Item2 },
            new Model.DriverStatus { DriverId = 2, Available = true,  LastSeen = _threeHoursAgo, Latitude =_spLatLng.Item1, Longitude = _spLatLng.Item2 },
            new Model.DriverStatus { DriverId = 3, Available = false, LastSeen = _tenMinutesAgo, Latitude =_spLatLng.Item1, Longitude = _spLatLng.Item2 },
            new Model.DriverStatus { DriverId = 4, Available = false, LastSeen = _threeHoursAgo, Latitude =_rjLatLnt.Item1, Longitude = _rjLatLnt.Item2 },
            new Model.DriverStatus { DriverId = 5, Available = true,  LastSeen = _tenMinutesAgo, Latitude =_rjLatLnt.Item1, Longitude = _rjLatLnt.Item2 }
        }.ToList();


        protected override void Seed(TaxisContext context)
        {
            DriversToSeed.ForEach(driver => context.Drivers.Add(driver));
            DriversStatuses.ForEach(status => context.DriversStatuses.Add(status));
            base.Seed(context);
        }
    }
}
