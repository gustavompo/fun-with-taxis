﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FunWithTaxis.Repository.Model
{
    public class Driver
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity) ]
        public int DriverId { get; set; }
        public string Name { get; set; }

        [Required, RegularExpression(@"\w{3}\-\d{4}")]
        public string CarPlate { get; set; }

        public virtual DriverStatus Status { get; set; }
    }
}
