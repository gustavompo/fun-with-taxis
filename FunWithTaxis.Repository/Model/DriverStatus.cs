﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace FunWithTaxis.Repository.Model
{
    public class DriverStatus
    {
        [Key, ForeignKey("Driver")]
        public int DriverId { get; set; }
     
        public virtual Driver Driver { get; set; }

        public DateTime LastSeen { get; set; }

        [Index("IX_LastSeenLat", 2)]
        public double Latitude { get; set; }

        [Index("IX_LastSeenLng", 3)]
        public double Longitude { get; set; }

        [Index("IX_Available", 1)]
        public bool Available { get; set; }

        public void Update(DriverStatus other)
        {
            Available = other.Available;
            LastSeen = other.LastSeen;
            Latitude = other.Latitude;
            Longitude = other.Longitude;
        }
    }
}
